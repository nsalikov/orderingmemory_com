# -*- coding: utf-8 -*-

# Scrapy settings for orderingmemory_com project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

import os
from shutil import which


CWD = os.path.dirname(os.path.realpath(__file__))

BOT_NAME = 'orderingmemory_com'

SPIDER_MODULES = ['orderingmemory_com.spiders']
NEWSPIDER_MODULE = 'orderingmemory_com.spiders'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
DEFAULT_REQUEST_HEADERS = {
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
  'Accept-Language': 'en-US,en;q=0.5',
  'Accept-Encoding': 'gzip',
}

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'orderingmemory_com.middlewares.OrderingmemoryComSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    'orderingmemory_com.middlewares.OrderingmemoryComDownloaderMiddleware': 543,
#}

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
# ITEM_PIPELINES = {
#    'orderingmemory_com.pipelines.Pipeline': 300,
# }

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

FEED_EXPORT_ENCODING = 'utf-8'

CONCURRENT_REQUESTS = 80
CONCURRENT_REQUESTS_PER_DOMAIN = 40
CONCURRENT_REQUESTS_PER_IP = 40

AUTOTHROTTLE_ENABLED = True
AUTOTHROTTLE_START_DELAY = 5
AUTOTHROTTLE_MAX_DELAY = 30
AUTOTHROTTLE_TARGET_CONCURRENCY = 10.0
DOWNLOAD_DELAY = 0

DOWNLOAD_TIMEOUT = 60

HTTPCACHE_ENABLED = False

MODEL_FILE = 'model.jl'
MODEL_FILE = os.path.join(CWD, '..', 'data', MODEL_FILE)

PROCESSOR_FILE = 'processor.jl'
PROCESSOR_FILE = os.path.join(CWD, '..', 'data', PROCESSOR_FILE)

MODEL_PROCESSOR_INFO_FILE = 'model_processor_info.jl'
MODEL_PROCESSOR_INFO_FILE = os.path.join(CWD, '..', 'data', MODEL_PROCESSOR_INFO_FILE)

MODEL_PROCESSOR_MAP_FILE = 'model_processor_map.json'
MODEL_PROCESSOR_MAP_FILE = os.path.join(CWD, '..', 'data', MODEL_PROCESSOR_MAP_FILE)

CONFIG_DENSITY_FILE = 'config_density_map.json'
CONFIG_DENSITY_FILE = os.path.join(CWD, '..', 'data', CONFIG_DENSITY_FILE)

MEMORY_TYPE_FILE = 'memory_type_map.json'
MEMORY_TYPE_FILE = os.path.join(CWD, '..', 'data', MEMORY_TYPE_FILE)

MODULE_DENSITY_FILE = 'module_density_map.json'
MODULE_DENSITY_FILE = os.path.join(CWD, '..', 'data', MODULE_DENSITY_FILE)

CONFIG_FILE = 'config_map.json'
CONFIG_FILE = os.path.join(CWD, '..', 'data', CONFIG_FILE)
