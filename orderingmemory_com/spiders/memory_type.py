# -*- coding: utf-8 -*-
import re
import json
import copy
import scrapy

from scrapy.shell import inspect_response


class MemoryTypeSpider(scrapy.Spider):
    name = 'memory_type'
    allowed_domains = ['orderingmemory.com']

    custom_settings = {
        'ITEM_PIPELINES': {
           'orderingmemory_com.pipelines.MemoryTypePipeline': 300,
        }
    }


    def start_requests(self):
        url = 'https://b2b.orderingmemory.com/serverconfig/rest/services/configurator?mt=memory_type&max_density={}&no_of_proc={}&proc_model_id={}&server_model_id={}&bsn=crucialstandard'
        with open(self.settings['CONFIG_DENSITY_FILE'], encoding='utf8') as fin:
            data = json.load(fin)
            for key, lst in data.items():
                model_id, processor_model_id, no_of_proc = key.split(',')
                for e in lst:
                    max_density = e['id']
                    key = ','.join([model_id, processor_model_id, no_of_proc, max_density])
                    meta = {'key': key}
                    yield scrapy.Request(url.format(max_density, no_of_proc, processor_model_id, model_id), meta=meta, callback=self.parse_memory_type)


    def parse_memory_type(self, response):
        # inspect_response(response, self)

        try:
            data = json.loads(response.text)
        except Exception as e:
            self.logger.warning('Unable to parse json: <{}>'.format(response.url), exc_info=True)
            self.logger.debug(response.text)
            return

        return {'url': response.url, 'key': response.meta['key'], 'data': data}
