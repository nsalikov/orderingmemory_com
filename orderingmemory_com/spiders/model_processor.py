# -*- coding: utf-8 -*-
import re
import json
import copy
import scrapy

from scrapy.shell import inspect_response


class ModelProcessorSpider(scrapy.Spider):
    name = 'model_processor'
    allowed_domains = ['orderingmemory.com']

    custom_settings = {
        'ITEM_PIPELINES': {
           'orderingmemory_com.pipelines.ModelProcessorPipeline': 300,
        }
    }


    def start_requests(self):
        with open(self.settings['MODEL_FILE'], encoding='utf8') as fin:
            for line in fin:
                d = json.loads(line)
                url = 'https://b2b.orderingmemory.com/serverconfig/rest/services/configurator?mt=proc_mfgr&server_model_id={}&bsn=crucialstandard&configid=noval'
                yield scrapy.Request(url.format(d['model_id']), meta={'model_id': d['model_id']}, callback=self.parse_processor_manufacturer)


    def parse_processor_manufacturer(self, response):
        #  inspect_response(response, self)

        try:
            data = json.loads(response.text)
        except Exception as e:
            self.logger.warning('Unable to parse json: <{}>'.format(response.url), exc_info=True)
            self.logger.debug(response.text)
            return

        processor_line_url = 'https://b2b.orderingmemory.com/serverconfig/rest/services/configurator?mt=proc_pl&mfgr={}&server_model_id={}&bsn=crucialstandard&configid=noval'

        for d in data:
            item = {'model_id': response.meta['model_id']}
            # item['processor_url'] = response.url
            item['processor_manufacturer_id'] = d['id']
            item['processor_manufacturer_name'] = d['value']

            yield scrapy.Request(processor_line_url.format(d['id'], item['model_id']), meta={'item': item}, callback=self.parse_processor_line)


    def parse_processor_line(self, response):
        #  inspect_response(response, self)

        try:
            data = json.loads(response.text)
        except Exception as e:
            self.logger.warning('Unable to parse json: <{}>'.format(response.url), exc_info=True)
            self.logger.debug(response.text)
            return

        processor_model_url = 'https://b2b.orderingmemory.com/serverconfig/rest/services/configurator?mt=proc_model&pl={}&server_model_id={}&bsn=crucialstandard&configid=noval'

        for d in data:
            item = copy.deepcopy(response.meta['item'])
            # item['processor_line_url'] = response.url
            item['processor_line_id'] = d['id']
            item['processor_line_name'] = d['value']

            yield scrapy.Request(processor_model_url.format(d['id'], item['model_id']), meta={'item': item}, callback=self.parse_processor_model)


    def parse_processor_model(self, response):
        #  inspect_response(response, self)

        try:
            data = json.loads(response.text)
        except Exception as e:
            self.logger.warning('Unable to parse json: <{}>'.format(response.url), exc_info=True)
            self.logger.debug(response.text)
            return

        number_of_processors_url = 'https://b2b.orderingmemory.com/serverconfig/rest/services/configurator?mt=proc_core&proc_model_id={}&bsn=crucialstandard&configid=noval'

        for d in data:
            item = copy.deepcopy(response.meta['item'])
            # item['processor_model_url'] = response.url
            item['processor_model_id'] = d['id']
            item['processor_model_name'] = d['value']

            yield scrapy.Request(number_of_processors_url.format(d['id']), meta={'item': item}, callback=self.parse_number_of_processors, dont_filter=True)


    def parse_number_of_processors(self, response):
        #  inspect_response(response, self)

        try:
            data = json.loads(response.text)
        except Exception as e:
            self.logger.warning('Unable to parse json: <{}>'.format(response.url), exc_info=True)
            self.logger.debug(response.text)
            return

        model_info_url = 'https://b2b.orderingmemory.com/serverconfig/rest/services/configurator?mt=model_info&server_model_id={}&proc_model_id={}&bsn=crucialstandard'

        item = copy.deepcopy(response.meta['item'])
        # item['total_number_of_processors_url'] = response.url
        item['total_number_of_processors'] = [d['value'] for d in data]

        yield scrapy.Request(model_info_url.format(item['model_id'], item['processor_model_id']), meta={'item': item}, callback=self.parse_model_info)


    def parse_model_info(self, response):
        #  inspect_response(response, self)

        try:
            data = json.loads(response.text)
        except Exception as e:
            self.logger.warning('Unable to parse json: <{}>'.format(response.url), exc_info=True)
            self.logger.debug(response.text)
            return

        item = copy.deepcopy(response.meta['item'])
        # item['model_processor_info_url'] = response.url
        item['model_specs'] = data[0]
        item['processor_specs'] = data[1]

        return item
