# -*- coding: utf-8 -*-
import re
import json
import copy
import scrapy

from scrapy.shell import inspect_response


class ModuleDensitySpider(scrapy.Spider):
    name = 'module_density'
    allowed_domains = ['orderingmemory.com']

    custom_settings = {
        'ITEM_PIPELINES': {
           'orderingmemory_com.pipelines.ModuleDensityPipeline': 300,
        }
    }


    def start_requests(self):
        url = 'https://b2b.orderingmemory.com/serverconfig/rest/services/configurator?mt=module_density&bsn=crucialstandard&mem_type={}&proc_model_id={}&max_density={}&no_of_proc={}&server_model_id={}'
        with open(self.settings['MEMORY_TYPE_FILE'], encoding='utf8') as fin:
            data = json.load(fin)
            for key, mem_types in data.items():
                model_id, processor_model_id, no_of_proc, max_density = key.split(',')

                disabled = [d['id'] for d in mem_types if d.get('enable', 'NA') == 'no']
                enabled = list(set([d['id'] for d in mem_types if d['id'] not in disabled]))

                for mem_type in enabled:
                    new_key = ','.join([model_id, processor_model_id, no_of_proc, max_density, mem_type])
                    meta = {'key': new_key}
                    yield scrapy.Request(url.format(mem_type, processor_model_id, max_density, no_of_proc, model_id), meta=meta, callback=self.parse_memory_density)


    def parse_memory_density(self, response):
        #  inspect_response(response, self)

        try:
            data = json.loads(response.text)
        except Exception as e:
            self.logger.warning('Unable to parse json: <{}>'.format(response.url), exc_info=True)
            self.logger.debug(response.text)
            return

        return {'url': response.url, 'key': response.meta['key'], 'data': data}
