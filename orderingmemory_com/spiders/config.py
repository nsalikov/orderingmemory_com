# -*- coding: utf-8 -*-
import re
import json
import copy
import scrapy

from scrapy.shell import inspect_response


class ConfigSpider(scrapy.Spider):
    name = 'config'
    allowed_domains = ['orderingmemory.com']

    custom_settings = {
        'ITEM_PIPELINES': {
           'orderingmemory_com.pipelines.ConfigPipeline': 300,
        }
    }


    def start_requests(self):
        url = 'https://b2b.orderingmemory.com/serverconfig/rest/services/configurator?mt=config_slots&bsn=crucialstandard&proc_model_id={}&mem_type={}&mod_den={}'
        with open(self.settings['MODULE_DENSITY_FILE'], encoding='utf8') as fin:
            data = json.load(fin)
            for key, modules in data.items():
                model_id, processor_model_id, no_of_proc, max_density, mem_type = key.split(',')

                disabled = [d['id'] for d in modules if d.get('enable', 'NA') == 'no']
                enabled = list(set([d['id'] for d in modules if d['id'] not in disabled]))

                for mod_density in enabled:
                    new_key = ','.join([processor_model_id, mem_type, mod_density])
                    meta = {'key': new_key}
                    yield scrapy.Request(url.format(processor_model_id, mem_type, mod_density), meta=meta, callback=self.parse_config_slots)



    def parse_config_slots(self, response):
        #  inspect_response(response, self)

        try:
            data = json.loads(response.text)
        except Exception as e:
            self.logger.warning('Unable to parse json: <{}>'.format(response.url), exc_info=True)
            self.logger.debug(response.text)
            return

        return {'url': response.url, 'key': response.meta['key'], 'data': data}
