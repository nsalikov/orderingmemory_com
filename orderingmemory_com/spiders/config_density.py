# -*- coding: utf-8 -*-
import re
import json
import copy
import scrapy

from scrapy.shell import inspect_response


class ConfigDensitySpider(scrapy.Spider):
    name = 'config_density'
    allowed_domains = ['orderingmemory.com']

    custom_settings = {
        'ITEM_PIPELINES': {
           'orderingmemory_com.pipelines.ConfigDensityPipeline': 300,
        },
        'HTTPCACHE_ENABLED': False,
    }


    def start_requests(self):
        url = 'https://b2b.orderingmemory.com/serverconfig/rest/services/configurator?mt=config_density&server_model_id={}&proc_model_id={}&no_of_proc={}&bsn=crucialstandard'
        with open(self.settings['MODEL_PROCESSOR_INFO_FILE'], encoding='utf8') as fin:
            for line in fin:
                d = json.loads(line)
                model_id = d['model_id']
                processor_model_id = d['processor_model_id']
                for no_of_proc in d['total_number_of_processors']:
                    key = ','.join([model_id, processor_model_id, no_of_proc])
                    meta = {'key': key}
                    yield scrapy.Request(url.format(model_id, processor_model_id, no_of_proc), meta=meta, callback=self.parse_memory)


    def parse_memory(self, response):
        # inspect_response(response, self)

        try:
            data = json.loads(response.text)
        except Exception as e:
            self.logger.warning('Unable to parse json: <{}>'.format(response.url), exc_info=True)
            self.logger.debug(response.text)
            return

        return {'url': response.url, 'key': response.meta['key'], 'data': data}
