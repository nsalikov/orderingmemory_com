# -*- coding: utf-8 -*-
import re
import json
import copy
import scrapy

from scrapy.shell import inspect_response


class ModelSpider(scrapy.Spider):
    name = 'model'
    allowed_domains = ['orderingmemory.com']
    start_urls = ['https://b2b.orderingmemory.com/serverconfig/rest/services/configurator?mt=server_mfgr&bsn=crucialstandard&configid=noval']

    custom_settings = {
        'ITEM_PIPELINES': {
           'orderingmemory_com.pipelines.ModelPipeline': 300,
        }
    }


    def parse(self, response):
        #  inspect_response(response, self)

        try:
            data = json.loads(response.text)
        except Exception as e:
            self.logger.warning('Unable to parse json: <{}>'.format(response.url), exc_info=True)
            self.logger.debug(response.text)
            return

        line_url = 'https://b2b.orderingmemory.com/serverconfig/rest/services/configurator?mt=server_pl&mfgr={}&bsn=crucialstandard&configid=noval'

        for d in data:
            item = {
                    # 'manufacturer_url': response.url,
                    'manufacturer_id': d['id'],
                    'manufacturer_name': d['value']
            }
            yield scrapy.Request(line_url.format(d['id']), meta={'item': item}, callback=self.parse_line)


    def parse_line(self, response):
        #  inspect_response(response, self)

        try:
            data = json.loads(response.text)
        except Exception as e:
            self.logger.warning('Unable to parse json: <{}>'.format(response.url), exc_info=True)
            self.logger.debug(response.text)
            return

        model_url = 'https://b2b.orderingmemory.com/serverconfig/rest/services/configurator?mt=server_model&pl={}&bsn=crucialstandard&configid=noval'

        for d in data:
            item = copy.deepcopy(response.meta['item'])
            # item['line_url'] = response.url
            item['line_id'] = d['id']
            item['line_name'] = d['value']

            yield scrapy.Request(model_url.format(d['id']), meta={'item': item}, callback=self.parse_model)


    def parse_model(self, response):
        #  inspect_response(response, self)

        try:
            data = json.loads(response.text)
        except Exception as e:
            self.logger.warning('Unable to parse json: <{}>'.format(response.url), exc_info=True)
            self.logger.debug(response.text)
            return

        for d in data:
            item = copy.deepcopy(response.meta['item'])
            # item['model_url'] = response.url
            item['model_id'] = d['id']
            item['model_name'] = d['value']

            yield item
