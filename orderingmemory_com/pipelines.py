# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import json


class ModelPipeline(object):

    model_file = None
    items = []


    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            model_file=crawler.settings.get('MODEL_FILE', None)
        )


    def __init__(self, model_file):
        self.model_file = model_file


    def process_item(self, item, spider):
        self.items.append(dict(item))
        return item


    def close_spider(self, spider):
        with open(self.model_file, 'w', encoding='utf-8') as fout:
            for item in self.items:
                fout.write(json.dumps(item) + '\n')



class ModelProcessorPipeline(object):

    processor_file = None
    model_processor_info_file = None
    model_processor_map_file = None

    processors = []
    model_processor_map = {}


    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            processor_file=crawler.settings.get('PROCESSOR_FILE', None),
            model_processor_info_file=crawler.settings.get('MODEL_PROCESSOR_INFO_FILE', None),
            model_processor_map_file=crawler.settings.get('MODEL_PROCESSOR_MAP_FILE', None),
        )


    def __init__(self, processor_file, model_processor_info_file, model_processor_map_file):
        self.processor_file = processor_file
        self.model_processor_info_file = model_processor_info_file
        self.model_processor_map_file = model_processor_map_file


    def process_item(self, item, spider):
        model_id = item['model_id']
        cpu_id = item['processor_model_id']

        d = dict(item)
        if model_id not in self.model_processor_map:
                self.model_processor_map[model_id] = []

        self.model_processor_map[model_id].append(cpu_id)
        self.processors.append(d)

        return item


    def close_spider(self, spider):
        with open(self.processor_file, 'w', encoding='utf-8') as fout:
            processors = {}
            for p in self.processors:
                if p['processor_model_id'] not in processors:
                    processors[p['processor_model_id']] = {
                        'processor_manufacturer_id': p['processor_manufacturer_id'],
                        'processor_manufacturer_name': p['processor_manufacturer_name'],
                        'processor_line_id': p['processor_line_id'],
                        'processor_line_name': p['processor_line_name'],
                        'processor_model_id': p['processor_model_id'],
                        'processor_model_name': p['processor_model_name'],
                    }
            for processor in processors.values():
                fout.write(json.dumps(processor) + '\n')

        with open(self.model_processor_info_file, 'w', encoding='utf-8') as fout:
            for processor in self.processors:
                fout.write(json.dumps(processor) + '\n')

        with open(self.model_processor_map_file, 'w', encoding='utf-8') as fout:
            model_processor_map = {k:sorted(list(set(v))) for k,v in self.model_processor_map.items()}
            fout.write(json.dumps(model_processor_map) + '\n')


class ConfigDensityPipeline(object):
    config_density_file = None
    items = []

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            config_density_file=crawler.settings.get('CONFIG_DENSITY_FILE', None)
        )


    def __init__(self, config_density_file):
        self.config_density_file = config_density_file


    def process_item(self, item, spider):
        self.items.append(dict(item))
        return item


    def close_spider(self, spider):
        with open(self.config_density_file, 'w', encoding='utf-8') as fout:
            config_density_map = {item['key']:item['data'] for item in self.items}
            fout.write(json.dumps(config_density_map) + '\n')


class MemoryTypePipeline(object):
    memoty_type_file = None
    items = []

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            memoty_type_file=crawler.settings.get('MEMORY_TYPE_FILE', None)
        )


    def __init__(self, memoty_type_file):
        self.memoty_type_file = memoty_type_file


    def process_item(self, item, spider):
        self.items.append(dict(item))
        return item


    def close_spider(self, spider):
        with open(self.memoty_type_file, 'w', encoding='utf-8') as fout:
            config_density_map = {item['key']:item['data'] for item in self.items}
            fout.write(json.dumps(config_density_map) + '\n')


class ModuleDensityPipeline(object):
    module_density_file = None
    items = []

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            module_density_file=crawler.settings.get('MODULE_DENSITY_FILE', None)
        )


    def __init__(self, module_density_file):
        self.module_density_file = module_density_file


    def process_item(self, item, spider):
        self.items.append(dict(item))
        return item


    def close_spider(self, spider):
        with open(self.module_density_file, 'w', encoding='utf-8') as fout:
            config_density_map = {item['key']:item['data'] for item in self.items}
            fout.write(json.dumps(config_density_map) + '\n')


class ConfigPipeline(object):
    config_file = None
    items = []

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            config_file=crawler.settings.get('CONFIG_FILE', None)
        )


    def __init__(self, config_file):
        self.config_file = config_file


    def process_item(self, item, spider):
        self.items.append(dict(item))
        return item


    def close_spider(self, spider):
        with open(self.config_file, 'w', encoding='utf-8') as fout:
            config_density_map = {item['key']:item['data'] for item in self.items}
            fout.write(json.dumps(config_density_map) + '\n')
