1. Run order:

```
scrapy crawl model
scrapy crawl model_processor
scrapy crawl config_density
scrapy crawl memory_type
scrapy crawl module_density
scrapy crawl config

```

2. The data:

```
cd data
ls -la
```

3. Map keys:

```
config_density_map
model_id,processor_model_id,no_of_proc

memory_type_map
model_id,processor_model_id,no_of_proc,max_density

module_density_map
model_id,processor_model_id,no_of_proc,max_density,mem_type

config_map
processor_model_id,mem_type,mod_density

```
